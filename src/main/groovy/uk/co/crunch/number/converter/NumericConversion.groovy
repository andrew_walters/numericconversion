package uk.co.crunch.number.converter

import static uk.co.crunch.number.converter.NumericConversion.findNextNumeralUp

class NumericConversion {
    static def swapKeyAndValue = { k, v -> [v, k] }
    static def ascendingByKey = { a, b -> a.key <=> b.key }
    static def nextValueUpAndMap = { val, k, v -> k > val ? [roman: v, arabic: k] : null }

    static def romanToArabicMap = [M: 1000, D: 500, C: 100, L: 50, X: 10, V: 5, I: 1]
    static def arabicToRomanMap = romanToArabicMap.collectEntries(swapKeyAndValue)
    static def findNextNumeralUp = { arabicToRomanMap.sort(ascendingByKey).findResult(nextValueUpAndMap.curry(it)) }

    static def arabicToRoman = {
        arabicToRomanMap.inject(value: it, result: "") { acc, arabic, roman ->
            def occurrences = (acc.value / arabic) as int

            if (occurrences == 4) {
                def replace = findNextNumeralUp arabic
                acc.value -= 4 * arabic

                if (acc.result.endsWith(replace.roman)) {
                    acc.result = acc.result - replace.roman + "$roman${findNextNumeralUp(replace.arabic).roman}"
                } else {
                    acc.result += "$roman$replace.roman"
                }
            } else {
                occurrences.times {
                    acc.result += roman
                    acc.value -= arabic
                }
            }

            [value: acc.value, result: acc.result]
        }.result
    }

    static def romanToArabic = {
        it.collect { c -> romanToArabicMap."$c"?.value }.reverse().inject(previous: 0, total: 0) { acc, val ->
            [previous: val, total: acc.total + (val >= acc.previous ? val : -val)]
        }.total
    }
}