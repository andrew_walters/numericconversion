package uk.co.crunch.number.converter

import spock.lang.Unroll

/**
 * Created by andrew.walters on 29/07/2014.
 */
class NumericConversionUnitTest extends spock.lang.Specification {

    @Unroll("Test an arabic input of #input converts to #expected")
    def "Test Arabic To Roman Conversion"() {
        given: "No specific setup is required"
        expect: "That an input of #input emits a result of #expected"
        NumericConversion.arabicToRoman(input) == expected
        where: "The input and expected values are"
        input || expected
        1 || 'I'
        2 || 'II'
        3 || 'III'
        4 || 'IV'
        5 || 'V'
        6 || 'VI'
        7 || 'VII'
        8 || 'VIII'
        9 || 'IX'
        10 || 'X'
        11 || 'XI'
        12 || 'XII'
        13 || 'XIII'
        14 || 'XIV'
        15 || 'XV'
        100 || 'C'
        1000 || 'M'
        1666 || 'MDCLXVI'
        1997 || 'MCMXCVII'
        1998 || 'MCMXCVIII'
        1999 || 'MCMXCIX'
        2000 || 'MM'
    }

    @Unroll("Test a roman input of #input converts to #expected")
    def "Test Roman To Arabic Conversion"() {
        given: "No specific setup is required"
        expect: "That an input of #input emits a result of #expected"
        NumericConversion.romanToArabic(input) == expected
        where: "The input and expected values are"
        input       ||  expected
        'I'         ||  1
        'II'        ||  2
        'III'       ||  3
        'IV'        ||  4
        'V'         ||  5
        'VI'        ||  6
        'VII'       ||  7
        'VIII'      ||  8
        'IX'        ||  9
        'X'         ||  10
        'XI'        ||  11
        'XII'       ||  12
        'XIII'      ||  13
        'XIV'       ||  14
        'XV'        ||  15
        'C'         ||  100
        'M'         ||  1000
        'MDCLXVI'   ||  1666
        'MCMXCVII'  ||  1997
        'MCMXCVIII' ||  1998
        'MCMXCIX'   ||  1999
        'MM'        ||  2000
    }

    @Unroll("Test the conversion between roman and arabic agrees for the value #val")
    def "Equality testing"() {
        given: "No specific setup is required"
        expect: "That the result of the arabic to roman computation applied on the result of the roman to arabic computation yields the input parameter"
        NumericConversion.romanToArabic(NumericConversion.arabicToRoman(val)) == val
        where: "We iterate through all integers from 1 to 2000"
        val << (1..2000)
    }
}
